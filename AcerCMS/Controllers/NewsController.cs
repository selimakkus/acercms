﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new List<News>();
            using (var db = new AcerCmsEntities())
            {
                model = db.News.OrderByDescending(o=> o.UpdateDate).ToList();
            }
            return View(model);
        }

        public ActionResult ReadNews(int id)
        {
            using (var db= new AcerCmsEntities())
            {
                var model = db.News.Find(id);

                if (model == null)
                    return HttpNotFound("Haber Bulunamadı!");
                
                return View(model);
            }
        }

    }
}